Functional Medicine is the medical practice or treatments that focus on optimal functioning of the body and its organs. It is the future of conventional medicine - but available now through Bliss Medicine. For more information call (630) 289-0440.

Address: 1802 Irving Park Rd, Hanover Park, IL 60133, USA

Phone: 630-289-0440